//
// POSTS
//
function getPosts() {
    var link = 'https://www.future-processing.pl/blog/wp-json/wp/v2/posts?per_page=10';
    $.getJSON(link, function(data){
        var time_start = Date.now();
        var posts = '';
        for(i=0; i<data.length; i++){

            var date = new Date(data[i].date);
            var date_format = date.format("yyyy-mm-dd");

            var title = JSON.stringify(data[i].title);
            title = title.replace(/{"rendered":"/g, "");
            title = title.replace(/"}/g,"");

            var content = JSON.stringify(data[i].content);
            var content_slice = content.slice(0,400).replace(/<\/?[^>]+>/gi, '');
            content_slice += "...";
            content_slice = content_slice.replace(/{"rendered":"/g, "");
            content_slice = content_slice.replace(/\\n/g, " ");

            var post_link = data[i].link;

            posts = posts + "<div  data-aos='fade-right' data-aos-delay='200' class='post post-" + i + "'><div class='post-content'> " +
                "<h4>" + title + "</h4>" +
                "<p class='post-date'>POSTED ON " + date_format + "</p>" +
                "<p class='post-text'>" + content_slice + "</p>" +
                "<a data-hover=\"READ MORE ...\" class='readmore-button' href='" + post_link + "'>&nbsp" +
                "</a></div></div>";
        }
        var time_end = Date.now();
        var time = time_end - time_start;
        if(time < 3000){
            setTimeout(function(){
                $('#load-posts').css('display', 'none');
                $('#posts').append(posts);
            } ,3000 - time);
        }
        else{
            $('#load-posts').css('display', 'none');
            $('#posts').append(posts);
        }

    })
}

function listOfPosts(){
    $('#loader').css('display', 'block');
    $('#load-posts-button').html('<button class="button button-black"><div class="button-text button-text-black animated fadeIn">Pobieram wpisy</div><span class="ellipsis">...</span></button>');
    getPosts();
}

//
// ANIMATIONS
//
function postsAnimation(){
    AOS.init({
        // Global settings:
        disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
        startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
        initClassName: 'aos-init', // class applied after initialization
        animatedClassName: 'aos-animate', // class applied on animation
        useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
        disableMutationObserver: false, // disables automatic mutations' detections (advanced)
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
        throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        offset: 100, // offset (in px) from the original trigger point
        delay: 0, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: true, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

    });
}