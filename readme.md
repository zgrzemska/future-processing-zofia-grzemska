Autor:
Zofia Grzemska
zosia.grzemska@gmail.com

Wykorzystane narzędzia i biblioteki frontendowe:
1. Bootstrap (https://getbootstrap.com/)
2. Animate.css (https://daneden.github.io/animate.css/)
3. AOS - Animate On Scroll (https://michalsnik.github.io/aos/)
4. jQuery (https://jquery.com/)
5. Sass (https://sass-lang.com/)
6. Date format (https://mvcworld.blogspot.com/2013/06/convert-json-date-to-javascript-date.html?fbclid=IwAR0AqUl814SRW2Jktf6klZSn5Xf9Zi8LNAJZZzOGVTooXhQGdg8_ukyvzRY)

Przesłany folder posiada wszystkie pliki niezbędne do uruchomienia projektu. Zostały zastosowane ścieżki względne, więc nie powinno być problemów z załadowaniem wszystkich elementów.